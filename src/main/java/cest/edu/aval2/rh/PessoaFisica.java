package cest.edu.aval2.rh;

public class PessoaFisica implements IPessoa{
    private String nome;
    private String CPF;
    private Endereco endereco;



    @Override
    public String getNome() {
        return this.nome;
    }

    public PessoaFisica(String nome, String CPF) {
        this.nome = nome;
        this.CPF = CPF;

    }

    @Override
    public String getNumeroIdentificador() {
        return this.CPF;
    }

    @Override
    public Endereco getEndereco() {
        return this.endereco;
    }

}
