package cest.edu.aval2.rh;

public interface IPessoa {
    public String getNome();
    public String getNumeroIdentificador();
    public Endereco getEndereco();

}
