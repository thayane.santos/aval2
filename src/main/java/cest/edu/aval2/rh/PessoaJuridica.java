package cest.edu.aval2.rh;

public class PessoaJuridica implements IPessoa{
    private String nome;
    private String cnpj;
    private Endereco endereco;

    @Override
    public String getNome() {
        return this.nome;
    }

    @Override
    public String getNumeroIdentificador() {
        return this.cnpj;
    }

    @Override
    public Endereco getEndereco() {
        return this.endereco;

    }

    public PessoaJuridica(String nome, String cnpj) {
        this.nome = nome;
        this.cnpj = cnpj;

    }
}
