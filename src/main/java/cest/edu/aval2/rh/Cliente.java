package cest.edu.aval2.rh;

import java.util.List;

public class Cliente extends PessoaFisica{
    private List<Atendimento> atendimentos;

    public List<Atendimento> getAtendimentos() {
        return atendimentos;
    }

    public void setAtendimentos(List<Atendimento> atendimentos) {
        this.atendimentos = atendimentos;
    }

    public Cliente(String nome, String CPF) {
        super(nome, CPF);
    }
}
