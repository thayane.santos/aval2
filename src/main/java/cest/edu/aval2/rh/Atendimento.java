package cest.edu.aval2.rh;

import java.util.Date;
import java.util.List;

public class Atendimento {
    private Date data;
    private Integer numero;
    private List<Requisicao> requisicoes;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public List<Requisicao> getRequisicoes() {
        return requisicoes;
    }

    public void setRequisicoes(List<Requisicao> requisicoes) {
        this.requisicoes = requisicoes;
    }

    public Atendimento(Date data, Integer numero) {
        this.data = data;
        this.numero = numero;

    }
}
